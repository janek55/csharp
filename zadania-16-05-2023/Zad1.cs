﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
partial class Program
{
    static void Zad1()
    {
        Console.WriteLine("Zad 1 poziom 1 zadanie 1");
        Console.WriteLine("Podaj długość prostokąta: ");
        double dlugosc = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Podaj szerokość prostokąta: ");
        double szerokosc = Convert.ToDouble(Console.ReadLine());
        double pole = dlugosc * szerokosc;
        Console.WriteLine("Pole prostokąta wynosi: " + pole);
        Console.WriteLine("...");
    }
}   