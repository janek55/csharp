﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
partial class Program
{
    static void Zad7()
    {
        Console.WriteLine("Zad 7 poziom 1 zadanie 4");
        Console.WriteLine("Podaj liczbę: ");
        int liczba = Convert.ToInt32(Console.ReadLine());
        if (liczba % 3 == 0 && liczba % 5 == 0)
        {
            Console.WriteLine("Liczba jest podzielna przez 3, jak i przez 5.");
        }
        else
        {
            Console.WriteLine("Liczba nie jest podzielna przez 3, jak i przez 5.");
        }
        Console.WriteLine("...");
    }
}