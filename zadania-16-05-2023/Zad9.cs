﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
partial class Program
{
    static void Zad9()
    {
        Console.WriteLine("Zad 9 poziom 2 zadanie 9");
        Console.WriteLine("Podaj liczbe");
        int liczba = Convert.ToInt32(Console.ReadLine());
        int sumaDzielnikow = 0;

        for (int i = 1; i < liczba; i++)
        {
            if (liczba % i == 0)
            {
                sumaDzielnikow += i;
            }
        }

        if (sumaDzielnikow == liczba)
        {
            Console.WriteLine("Podana liczba jest doskonała.");
        }
        else
        {
            Console.WriteLine("Podana liczba nie jest doskonała.");
        }
        Console.WriteLine("...");
    }
}