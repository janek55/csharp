﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
partial class Program
{
    static void Zad5()
    {
        Console.WriteLine("Zad 5 poziom 2 zadanie 2");
        Console.WriteLine("Podaj temperaturę w stopniach Celsiusza: ");
        double celsius = Convert.ToDouble(Console.ReadLine());
        double fahrenheit = ((celsius * 9 / 5)+32);
        Console.WriteLine("Temperatura w stopniach Fahrenheit: " + fahrenheit);
        Console.WriteLine("...");
    }
}