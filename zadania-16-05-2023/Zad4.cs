﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
partial class Program
{
    static void Zad4()
    {
        Console.WriteLine("Zad 4 poziom 2 zadanie 1");
        Console.WriteLine("Podaj pierwszą liczbę: ");
        double liczba1 = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Podaj drugą liczbę: ");
        double liczba2 = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Liczba 1: " + liczba1);
        Console.WriteLine("Liczba 2: " + liczba2);

        double suma = liczba1 + liczba2;
        double roznica = liczba1 - liczba2;
        double iloczyn = liczba1 * liczba2;
        double iloraz = liczba1 / liczba2;

        Console.WriteLine("Wynik dodawania: " + suma);
        Console.WriteLine("Wynik odejmowania: " + roznica);
        Console.WriteLine("Wynik mnożenia: " + iloczyn);

        if (double.IsInfinity(iloraz) || double.IsNaN(iloraz))
        {
            Console.WriteLine("Nie można dzielić przez zero!");
        }
        else
        {
            Console.WriteLine("Wynik dzielenia: " + iloraz);
        }
        Console.WriteLine(" ");
    }
}