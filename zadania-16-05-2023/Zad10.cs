﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

partial class Program
{
    static void Zad10()
    {
        Console.WriteLine("Zad 10 poziom 2 zadanie 8");
        Console.WriteLine("Podaj liczbę: ");
        int liczba = Convert.ToInt32(Console.ReadLine());
        int wynik = 1;
        for (int i = 2; i <= liczba; i++)
        {
            wynik *= i;
        }
        Console.WriteLine("Silnia dla liczby {0} wynosi: {1}", liczba, wynik);

        Console.ReadLine();
    }
}