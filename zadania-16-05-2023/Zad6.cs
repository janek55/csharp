﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
partial class Program
{
    static void Zad6()
    {
        Console.WriteLine("Zad 6 poziom 2 zadanie 7");
        Console.WriteLine("Podaj liczbę: ");
        int liczba = Convert.ToInt32(Console.ReadLine());
        bool pierwsza = true;
        if (liczba <= 1)
        {
            pierwsza = false;
        }
        else
        {
            int sqrt = (int)Math.Sqrt(liczba);

            for (int i = 2; i <= sqrt; i++)
            {
                if (liczba % i == 0)
                {
                    pierwsza = false;
                    break;
                }
            }
        }
        if (pierwsza)
        {
            Console.WriteLine("Podana liczba jest liczbą pierwszą.");
        }
        else
        {
            Console.WriteLine("Podana liczba nie jest liczbą pierwszą.");
        }
        Console.WriteLine("...");
    }
}