﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
partial class Program
{
    static void Zad3()
    {
        Console.WriteLine("Zad 3 poziom 1 zadanie 3");
        Console.WriteLine("Podaj pierwszą liczbę: ");
        int a = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Podaj drugą liczbę: ");
        int b = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Przed zamianą: a = " + a + ", b = " + b);
        int temp = a;
        a = b;
        b = temp;
        Console.WriteLine("Po zamianie: a = " + a + ", b = " + b);
        Console.WriteLine("...");
    }
}   