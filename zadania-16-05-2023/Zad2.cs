﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
partial class Program
{
    static void Zad2()
    {
        Console.WriteLine("Zad 2 poziom 1 zadanie 2");
        Console.WriteLine("Podaj liczbę: ");
        int liczba = Convert.ToInt32(Console.ReadLine());
        if (liczba % 2 == 0)
        {
            Console.WriteLine("Podana liczba jest parzysta.");
        }
        else
        {
            Console.WriteLine("Podana liczba nie jest parzysta.");
        }
        Console.WriteLine("...");
    }
}   